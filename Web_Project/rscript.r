#! /usr/bin/Rscript
library(comorbidity)
library(dplyr)
library(tidyverse)
library(lubridate)
rm(list = ls(all.names = TRUE))
armor_claims <- read.csv("claim.csv")
armor_charlson_data <- armor_claims %>% select(ID_NUMBER, DIAG_CD, BRTH_DT)
armor_unique <- armor_charlson_data [!duplicated(armor_charlson_data[c(1,2,3)]),]
armor_bd <- armor_claims %>% select(ID_NUMBER, BRTH_DT)
armor_bd <- armor_bd [!duplicated(armor_bd[c(1,2)]),]
armor_strat<- comorbidity(armor_unique, id = "ID_NUMBER", code = "DIAG_CD", score = "charlson", assign0 = FALSE)
# armor_strat$bth_ft <- armor_strat %>% left_join(armor_unique, by = c("ID_NUMBER"))
# armor_strat <- merge(x= armor_strat, y= armor_bd, "ID_NUMBER", all.x=TRUE)
# print(armor_strat)
# armor_strat$BRTH_DT <- as.Date(armor_strat$BRTH_DT, "%m/%d/%Y")
# armor_strat$Today <- Sys.Date()
# armor_strat$Age <- (armor_strat$Today - armor_strat$BRTH_DT)/365
# armor_strat$Age_Weight <- ifelse(armor_strat$Age <50, 0,
#                           ifelse(armor_strat$Age >= 50 & armor_strat$Age < 60, 1,
#                                  ifelse(armor_strat$Age >= 60 & armor_strat$Age < 69, 2,
#                                         ifelse(armor_strat$Age >= 70 & armor_strat$Age < 79, 3,
#                                                ifelse(armor_strat$Age >= 80, 4,0
#                                                       )))))
# # armor_strat$final_score <- armor_strat$Age_Weight + armor_strat$wscore
write.csv(armor_strat, "armor_strat_2.csv")