import pandas as pd

from .models import CsvFiles


class BulkLoadDataCoverter:

    def __init__(self, model, data):
        self.model = model
        self.data = data
        self.fields = [getattr(inst, "name") for inst in self.__get_all_model_fields()]

    def __get_all_model_fields(self):
        return self.model._meta.fields

    def __create_model_obj_list(self, row):
        return self.model(**dict(zip(self.fields[1:-1], row)))

    def __create_model_fields_list(self, row):
        return dict(zip(self.fields[1:-1], row))

    def validate_column_name(self):
        colums_name_org = self.data.columns.values.tolist()
        colums_name_new = ["".join(s.split(" ")).lower() for s in colums_name_org]
        self.data.rename(dict(zip(colums_name_org, colums_name_new)), axis=1, inplace=True)
        # diff = set(colums_name_new).difference(set(self.fields))
        diff = set(self.fields[1:-1]).difference(set(colums_name_new))
        print(diff)
        return diff

    def create_or_update(self, id=False):
        colums_name_org = self.data.columns.values.tolist()
        colums_name_new = ["".join(s.split(" ")).lower() for s in colums_name_org]
        self.data.rename(dict(zip(colums_name_org, colums_name_new)), axis=1, inplace=True)
        df1 = self.data.astype(object).where(pd.notnull(self.data), None)
        list_obj = df1[self.fields[1:-1]].apply(self.__create_model_fields_list, axis=1).tolist()
        for obj in list_obj:
            self.model.objects.update_or_create(**obj)
        if id:
            fileobj = CsvFiles.objects.get(pk=id)
            fileobj.uploading_status = False
            fileobj.save()



    def bulk_save(self):
        colums_name_org = self.data.columns.values.tolist()
        colums_name_new = ["".join(s.split(" ")).lower() for s in colums_name_org]
        self.data.rename(dict(zip(colums_name_org, colums_name_new)), axis=1, inplace=True)
        df1 = self.data.astype(object).where(pd.notnull(self.data), None)
        list_obj = df1[self.fields[1:-1]].apply(self.__create_model_obj_list, axis=1).tolist()
        try:
            self.model.objects.bulk_create(list_obj, batch_size=2000, ignore_conflicts=True)
        except Exception as e:
            print(e)


def load_data_from_objfile(file_object, sheet_name=None):
    ext = {"xlsx": pd.read_excel,
           "csv": pd.read_csv, }
    try:
        data = (ext[file_object.name.split(".")[-1]](file_object) if sheet_name == None
                else ext[file_object.name.split(".")[-1]](file_object, sheet_name=sheet_name))
    except KeyError:
        data = False
    data.drop_duplicates(inplace=True)
    return data

def load_data_from_file(file_object, sheet_name=None):
    ext = {"xlsx": pd.read_excel,
           "csv": pd.read_csv, }
    try:
        data = ext[file_object.split(".")[-1]](file_object)
    except KeyError:
        data = False
    data.drop_duplicates(inplace=True)
    return data

