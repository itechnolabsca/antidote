import uuid
import jsonfield
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from .querysets import VDOC_ADPQuerySet, ClaimsDataArmorQueryset


class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)


    def __str__(self):
        return self.title


class DummyModel(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()


class ContactUs(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    email = models.EmailField(verbose_name="email", max_length=500)
    subject = models.CharField(max_length=500, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.name


class CsvFiles(models.Model):
    csv = models.FileField(upload_to='static/csv')
    file_name = models.CharField(max_length=200)
    user_id = models.CharField(max_length=500)
    Status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    uploading_status = models.BooleanField(default=True)
    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.file_name


class Role(models.Model):
    user_id = models.CharField(max_length=500)
    role = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class Armor_Start(models.Model):
    id_number = models.IntegerField(null=True, blank=True)
    ami = models.BooleanField(default=False)
    chf = models.BooleanField(default=False)
    pvd = models.BooleanField(default=False)
    cevd = models.BooleanField(default=False)
    dementia = models.BooleanField(default=False)
    copd = models.BooleanField(default=False)
    rheumd = models.BooleanField(default=False)
    pud = models.BooleanField(default=False)
    mld = models.BooleanField(default=False)
    diab = models.BooleanField(default=False)
    diabwc = models.BooleanField(default=False)
    hp = models.BooleanField(default=False)
    rend = models.BooleanField(default=False)
    canc = models.BooleanField(default=False)
    msld = models.BooleanField(default=False)
    metacanc = models.BooleanField(default=False)
    aids = models.BooleanField(default=False)
    score = models.IntegerField(blank=True, null=True,default=None)
    index = models.CharField(max_length=100, null=True, blank=True, default=None)
    wscore = models.IntegerField(blank=True, null=True, default=None)
    windex = models.CharField(max_length=100, null=True, blank=True,default=None)
    brth_dt = models.DateField(null=True, blank=True,default=None)
    age = models.FloatField(null=True, blank=True, default=None)
    today = models.DateField(null=True, blank=True, default=None)
    age_weight = models.IntegerField(null=True, blank=True, default=None)
    final_score = models.IntegerField(null=True, blank=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class ClaimsDataArmor(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    claim_nbr = models.CharField(max_length=500, null=True, blank=True)
    clm_line_nbr = models.IntegerField(null=True, blank=True)
    service_code = models.CharField(max_length=500, null=True, blank=True,db_index=True)
    revnu_cd = models.CharField(max_length=500, null=True, blank=True)
    diag_cd = models.CharField(max_length=500, null=True, blank=True,db_index=True)
    drg_cd = models.CharField(max_length=500, null=True, blank=True)
    setting = models.CharField(max_length=500, null=True, blank=True,db_index=True)
    line_setting = models.CharField(max_length=500, null=True, blank=True)
    group_number = models.CharField(max_length=500, null=True, blank=True)
    group_name = models.CharField(max_length=500, null=True, blank=True,db_index=True)
    id_number = models.CharField(max_length=500, null=True, blank=True)
    # id_number = models.ForeignKey(Armor_Start, on_delete=models.CASCADE, null = True, blank = True)
    brth_dt = models.CharField(max_length=100, null=True, blank=True)
    gndr_cd = models.CharField(max_length=500, null=True, blank=True)
    c_fac_prof = models.CharField(max_length=500, null=True, blank=True)
    provider_name = models.CharField(max_length=500, null=True, blank=True,db_index=True)
    ndc = models.CharField(max_length=500, null=True, blank=True)
    drug_name = models.CharField(max_length=500, null=True, blank=True)
    date_incurred = models.CharField(max_length=200, null=True, blank=True)
    date_last_service = models.DateField(null=True, blank=True,db_index=True)
    total_charges = models.FloatField(null=True, blank=True)
    total_paid = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    objects = models.Manager()
    claims = ClaimsDataArmorQueryset.as_manager()

    def __str__(self):
        return "{}".format(self.claim_nbr)


class VDOC_ADP(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    site = models.CharField(max_length=500, null=True, blank=True)
    # group_name = models.ForeignKey(ClaimsDataArmor, on_delete=models.CASCADE, null=True)
    monthyear = models.CharField(max_length=500, null=True, blank=True)
    adp = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    objects = models.Manager()
    vdocs=VDOC_ADPQuerySet.as_manager()


class HCC(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # diag_cd = models.ForeignKey(ClaimsDataArmor, on_delete=models.CASCADE, null=True)
    icd = models.CharField(max_length=100, null=True, blank=True)
    icddesc = models.TextField(blank=True, null=True)
    icdvers = models.IntegerField(null=True, blank=True)
    hccesrd = models.FloatField(max_length=500, null=True, blank=True)
    hcc = models.FloatField(max_length=500, null=True, blank=True)
    hcc23 = models.FloatField(max_length=500, null=True, blank=True)
    rxhcc = models.FloatField(max_length=500, null=True, blank=True)
    inhccesrd = models.CharField(max_length=100, null=True, blank=True)
    inhcc = models.CharField(max_length=100, null=True, blank=True)
    inhcc23 = models.CharField(max_length=100, null=True, blank=True)
    inrxhcc = models.CharField(max_length=100, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    hccname = models.CharField(max_length=500, null=True, blank=True)
    mraweighting = models.FloatField(max_length=500, null=True, blank=True)
    hccdesc = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class ICD10(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # diag_cd = models.ForeignKey(ClaimsDataArmor, on_delete=models.CASCADE, null=True)
    fullicd10 = models.CharField(max_length=100, null=True, blank=True)
    indicator = models.CharField(max_length=50, null=True, blank=True)
    classcodes = models.CharField(max_length=500, null=True, blank=True)
    icd10description = models.CharField(max_length=500, null=True, blank=True)
    icdclass = models.CharField(max_length=500, null=True, blank=True)
    icd10field = models.CharField(max_length=500, null=True, blank=True)
    armorcategory = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class CPT(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # service_code = models.ForeignKey(ClaimsDataArmor, on_delete=models.CASCADE, null=True)
    procedurecode = models.CharField(max_length=500, null=True, blank=True)
    longdescription = models.TextField(null=True, blank=True)
    bucket = models.CharField(max_length=500, null=True, blank=True)
    classname = models.CharField(max_length=500, null=True, blank=True)
    fieldname = models.CharField(max_length=500, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class DRG(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # drg_cd = models.ForeignKey(ClaimsDataArmor, on_delete=models.CASCADE, null=True)
    drg = models.CharField(max_length=500, null=True, blank=True)
    mdc = models.CharField(max_length=500, null=True, blank=True)
    ms = models.CharField(max_length=500, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)


class JsonDocs(models.Model):
    JSON_TYPE = ((1,'Anomaly_Output'),(2,'Prediction_Output'))
    json_file_id = models.CharField(max_length=255,null=True,blank=True)
    doc_type = models.PositiveIntegerField(default=0,choices=JSON_TYPE)
    data = jsonfield.JSONField(default={})
    created_at = models.DateTimeField(auto_now_add=True)
