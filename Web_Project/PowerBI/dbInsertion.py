import pandas as pd

from .models import *


class DbInsertion():

	def datainsertion(self, data, c, file_type):
		if c == 0:
			if 'xlsx' in data:
				df = pd.read_excel(data, sheet_name='HCC')
				df2 = pd.read_excel(data, sheet_name='ICD')
			else:
				df = pd.read_csv(data, sheet_name='HCC')
				df2 = pd.read_csv(data, sheet_name='ICD')
			df = df.astype(object).where(pd.notnull(df), None)
			list_data = tuple()
			try:
				list_data = (HCC(diag_cd = row['icd'],
										icd_desc = row['icddesc'],
										icdvers = row['icdvers'],
										hcc_esrd = row['hccesrd'],
										hcc = row['hcc'],
										hcc23 = row['hcc23'],
										rxhcc = row['rxhcc'],
										in_hcc_esrd = row['inhccesrd'],
										in_hcc = row['inhcc'],
										in_hcc23 = row['inhcc23'],
										in_rxhcc = row['inrxhcc'],
										year = row['year'],
										hcc_name = row['HCC Name'],
										mra_weighting = row['MRA Weighting'],
										hcc_desc = row['HCC Desc'],) for index, row in df.iterrows() )
			except Exception as e:
				print(e)
				pass
			print(list_data)
			hcc, created = HCC.objects.bulk_create(list_data)
			# for i,row in df.iterrows():
			# 	try:
			# 		hcc, created = HCC.objects.update_or_create(
			# 							diag_cd__diag_cd = row['icd'],
			# 							icd_desc = row['icddesc'],
			# 							icdvers = row['icdvers'],
			# 							hcc_esrd = row['hccesrd'],
			# 							hcc = row['hcc'],
			# 							hcc23 = row['hcc23'],
			# 							rxhcc = row['rxhcc'],
			# 							in_hcc_esrd = row['inhccesrd'],
			# 							in_hcc = row['inhcc'],
			# 							in_hcc23 = row['inhcc23'],
			# 							in_rxhcc = row['inrxhcc'],
			# 							year = row['year'],
			# 							hcc_name = row['HCC Name'],
			# 							mra_weighting = row['MRA Weighting'],
			# 							hcc_desc = row['HCC Desc'],
			# 							)
			# 	except Exception as e:
			# 		print(e)
			# 		pass

			
			df2 = df2.astype(object).where(pd.notnull(df2), None)
			list_data = tuple()
			try:
				list_data = (ICD10(diag_cd = row['Full ICD 10'],
										indicator = row['Indicator'],
										class_codes = row['Class Codes'],
										icd10_desc = row['ICD 10 Description'],
										icd10_class = row['ICD Class'],
										icd10_field = row['ICD 10 Field'],
										armor_category = row['Armor Category'],) for index, row in df2.iterrows() )
			except Exception as e:
				print(e)
				pass
			print(list_data)
			icd10, created = ICD10.objects.bulk_create(list_data)
			# for i,row in df2.iterrows():
			# 	try:
			# 		icd10, created = ICD10.objects.update_or_create(
			# 							diag_cd__diag_cd = row['Full ICD 10'],
			# 							indicator = row['Indicator'],
			# 							class_codes = row['Class Codes'],
			# 							icd10_desc = row['ICD 10 Description'],
			# 							icd10_class = row['ICD Class'],
			# 							icd10_field = row['ICD 10 Field'],
			# 							armor_category = row['Armor Category'],)
			# 	except Exception as e:
			# 		print(e)
			# 		pass

		else:
            # df = pd.read_excel(file.name)
            # df.where(pd.notnull(df), None)
			if file_type == 'Armor':
				list_data = tuple()
				try:
					list_data = (Armor_Start(id_number= (int(row["ID_NUMBER"])), ami = row["ami"], chf= row["chf"],
										pvd = row["pvd"], cevd = row["cevd"], dementia = row["dementia"],
										copd= row["copd"], rheumd = row["rheumd"], pud = row["pud"],
										mld = row["mld"], diab = row["diab"], diabwc = row["diabwc"],
										hp = row["hp"], rend = row["rend"], canc = row["canc"],
										msld = row["msld"],metacanc = row["metacanc"],
										aids = row["aids"],score = row["score"],
										index = row["index"],wscore = row["wscore"],
										windex = row["windex"],birth_dt = row["BRTH_DT"],
										age = (float(row["Age"])),today = row["Today"],
										age_weight = row["Age_Weight"]
										,final_score = row["final_score"],) for index, row in data.iterrows() )
				except Exception as e:
					print(e)
					pass
				print(list_data)
				armor = Armor_Start.objects.bulk_create(list_data)
				# for i,row in data.iterrows():
				# 	try:
				# 		armor_start, created = Armor_Start.objects.update_or_create(
				# 						id_number= (int(row["ID_NUMBER"])), ami = row["ami"], chf= row["chf"],
				# 						pvd = row["pvd"], cevd = row["cevd"], dementia = row["dementia"],
				# 						copd= row["copd"], rheumd = row["rheumd"], pud = row["pud"],
				# 						mld = row["mld"], diab = row["diab"], diabwc = row["diabwc"],
				# 						hp = row["hp"], rend = row["rend"], canc = row["canc"],
				# 						msld = row["msld"],metacanc = row["metacanc"],
				# 						aids = row["aids"],score = row["score"],
				# 						index = row["index"],wscore = row["wscore"],
				# 						windex = row["windex"],birth_dt = row["BRTH_DT"],
				# 						age = (float(row["Age"])),today = row["Today"],
				# 						age_weight = row["Age_Weight"],final_score = row["final_score"],)
				# 	except Exception as e:
				# 		print(e)
				# 		pass

			elif file_type == 'VDOC_ADP':
				list_data = list()
				try:
					list_data = [VDOC_ADP(group_name = row["Site"],month_year = row['MonthYear'],
						adp = row['ADP']) for index, row in data.iterrows() ]
				except Exception as e:
					print(e)
					pass
				print(list_data)
				# for i,row in data.iterrows():
				# try:
				# 	vdoc, created = VDOC_ADP.objects.bulk_create(list_data)
				# 						# group_name__group_name= row["Site"],
				# 						# month_year = row["MonthYear"],
				# 						# adp = row["ADP"])
				# except Exception as e:
				# 	print(e)
				# 	pass

			elif file_type == 'Claims':
				list_data = tuple()
				try:
					list_data = (ClaimsDataArmor(claim_NBR = row['CLAIM_NBR'],
												claim_line_NBR = row['CLM_LINE_NBR'],
												service_code = row['SERVICE_CODE'],
												revnu_cd = row['REVNU_CD'],
												diag_cd = row['DIAG_CD'],
												drg_cd = row['DRG_CD'],
												setting = row['SETTING'],
												line_setting = row['LINE_SETTING'],
												group_number = row['GROUP_NUMBER'],
												group_name = row['GROUP_NAME'],
												id_number = row['ID_NUMBER'],
												birth_dt = row['BRTH_DT'],
												gndr_cd = row['GNDR_CD'],
												c_fac_prof = row['C_FAC_PROF'],
												provider_name = row['PROVIDER_NAME'],
												ndc = row['NDC'],
												drug_name = row['DRUG_NAME'],
												date_incured = row['DATE_INCURRED'],
												date_last_service = row['DATE_LAST_SERVICE'],
												total_charges = row['TOTAL_CHARGES'],
												total_paid = row[' TOTAL_PAID '],) for index, row in data.iterrows() )
				except Exception as e:
					print(e)
					pass
				print(list_data)
				ClaimsDataArmormodel, created = ClaimsDataArmor.objects.bulk_create(list_data)
				# for i,row in data.iterrows():
				# 	try:
				# ClaimsDataArmormodel, created = ClaimsDataArmor.objects.update_or_create(
				# 												claim_NBR = row['CLAIM_NBR'],
				# 												claim_line_NBR = row['CLM_LINE_NBR'],
				# 												service_code = row['SERVICE_CODE'],
				# 												revnu_cd = row['REVNU_CD'],
				# 												diag_cd = row['DIAG_CD'],
				# 												drg_cd = row['DRG_CD'],
				# 												setting = row['SETTING'],
				# 												line_setting = row['LINE_SETTING'],
				# 												group_number = row['GROUP_NUMBER'],
				# 												group_name = row['GROUP_NAME'],
				# 												id_number__id_number = row['ID_NUMBER'],
				# 												birth_dt = row['BRTH_DT'],
				# 												gndr_cd = row['GNDR_CD'],
				# 												c_fac_prof = row['C_FAC_PROF'],
				# 												provider_name = row['PROVIDER_NAME'],
				# 												ndc = row['NDC'],
				# 												drug_name = row['DRUG_NAME'],
				# 												date_incured = row['DATE_INCURRED'].date(),
				# 												date_last_service = row['DATE_LAST_SERVICE'].date(),
				# 												total_charges = row['TOTAL_CHARGES'],
				# 												total_paid = row[' TOTAL_PAID '],)
				# 	except Exception as e:
				# 		print(e)
				# 		pass

			elif file_type == 'CPT':
				for i,row in data.iterrows():
					try:
						CPTmodel, created = CPT.objects.update_or_create(
														service_code__service_code = row['Procedure Code'],
														long_desc = row['Long Description'],
														bucket = row['Bucket'],
														class_name = row['Class Name'],
														Field_name = row['Field Name'],
														)
					except Exception as e:
						print(e)
						pass

			elif file_type == 'DRG':
				for i,row in data.iterrows():
					try:
						DRGmodel, created = DRG.objects.update_or_create(
														drg_cd__drg_cd = row['DRG'],
														mdc = row['MDC'],
														ms = row['MS'],
														description = row['Description'],
														)
					except Exception as e:
						print(e)
						pass

