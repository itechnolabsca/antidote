import os
import threading

from Web_Project.settings import EMAIL_HOST_USER
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic.base import View

from .common import (asyload_to_db, normalize_adp_date,
                     site_drill_down_data)
from .forms import DateRangeFilterForm
from .graphs_data import *
# Create your views here.
from .utils import load_data_from_objfile, BulkLoadDataCoverter
from .models import JsonDocs
# from .tasks import asyload_to_db_task
team = [
    {
        'Member': 'Alex Campo',
        'Title': 'Director of Data Analytics',
    },
    {
        'Member': 'Manny Fernandez',
        'Title': 'Chief Operating Officer'
    },
    {
        'Member': 'Jennie Plotkin',
        'Title': 'VP of Information Systems'
    }
]


def home(request):
    content = {
        'posts': Post.objects.all()
    }
    return render(request, 'PowerBI/homepage.html', content)


def about(request):
    context = {
        'team': team
    }
    return render(request, 'PowerBI/about.html', context)


def access(request):
    return render(request, 'PowerBI/account_access.html')


@user_passes_test(lambda u: u.is_superuser , login_url='/login/')
def admin_dashboard(request):
    template = 'PowerBI/admin_dashboard.html'
    if request.method == 'GET':
        context = dict()
        cursor = connection.cursor()
        data = cursor.execute(
            "SELECT *  FROM  `auth_user` AS u INNER JOIN `PowerBI_role` AS r ON r.user_id = u.id WHERE  r.role = 1 ORDER BY u.date_joined DESC LIMIT 7")
        result = cursor.fetchall()
        print(result)
        data_list = list()
        for i in result:
            data_dict = dict()
            data_dict['id'] = i[0]
            data_dict['username'] = i[4]
            data_dict['email'] = i[7]
            data_dict['createdat'] = i[10]
            data_list.append(data_dict)

        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_armor_start")
        context['total_armor_data'] = ''.join(str(v) for v in cursor.fetchall())[1:-2]
        print(context['total_armor_data'])
        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_claimsdataarmor")
        context['total_claims_data'] = ''.join(str(v) for v in cursor.fetchall())[1:-2]
        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_cpt")
        context['total_cpt_data'] =''.join(str(v) for v in cursor.fetchall())[1:-2]
        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_drg")
        context['total_drg_data'] = ''.join(str(v) for v in cursor.fetchall())[1:-2]
        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_hcc")
        context['total_hcc_data'] = ''.join(str(v) for v in cursor.fetchall())[1:-2]
        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_icd10")
        context['total_icd10_data'] = ''.join(str(v) for v in cursor.fetchall())[1:-2]
        data = cursor.execute(
            "SELECT COUNT(*) FROM PowerBI_vdoc_adp")
        context['total_vdoc_data'] =''.join(str(v) for v in cursor.fetchall())[1:-2]
        # print(result.id)
        context["data"] = data_list
        return render(request, template, context)


@user_passes_test(lambda u: u.is_superuser , login_url='/login/')
def table(request):
    if request.method == "GET":
        context = dict()
        armorData = Armor_Start.objects.all()
        paginator = Paginator(armorData, 10)  # 3 posts in each page
        page = request.GET.get('page')
        try:
            armor = paginator.page(page)
        except PageNotAnInteger:
            armor = paginator.page(1)
        except EmptyPage:
            armor = paginator.page(paginator.num_pages)
        context = {"armor": armor, 
                    'page': page,
                }
        return render(request, 'PowerBI/ui-tables.html', context)
    if request.method == "POST":
        mapping = {"Armor": Armor_Start, "VDOC_ADP": VDOC_ADP, "Claims": ClaimsDataArmor, "CPT": CPT, "DRG": DRG}
        try:
            file = request.FILES['file'].file
        except:
            return redirect('/admin_dashboard/table')
        try:
            file_type = request.POST.get('file_name')
        except:
            return redirect('/admin_dashboard/table')
        if file.name.split(".")[-1] not in ["xlsx", "csv"]:
            return redirect('/admin_dashboard/table')
        if file_type == "ICD10":
            data1 = load_data_from_objfile(file, sheet_name="HCC")
            data2 = load_data_from_objfile(file, sheet_name="ICD")
            converter1 = BulkLoadDataCoverter(HCC, data1)
            if converter1.validate_column_name():
                messages.error(request, "File and file type should be same. Kindly upload {0} file or missing fields {1}".format(file_type,converter1.validate_column_name()))
                return redirect('/admin_dashboard/table')
            converter1.bulk_save()
            converter = BulkLoadDataCoverter(ICD10, data2)
            if converter.validate_column_name():
                messages.error(request, "File and file type should be same. Kindly upload {0} file or missing fields {1}"
                               .format(file_type,converter.validate_column_name()))
                return redirect('/admin_dashboard/table')
            converter.bulk_save()
            request.session["status"] = "completed"
        elif file_type == "Claims":
            print("reading file")
            # file_abs_name=os.path.join(settings.MEDIA_ROOT,file.name)
            # print(file.name)
            # with open()
            # asyload_to_db_task.delay(file.name,file_type,False)
            data = load_data_from_objfile(file)
            converter = BulkLoadDataCoverter(mapping[file_type], data[40000:])
            if converter.validate_column_name():
                messages.error(request, "File and file type should be same. Kindly upload {0} file or missing fields {1}"
                               .format(file_type,converter.validate_column_name()))
                return redirect('/admin_dashboard/table')
            converter.bulk_save()
        else:
            request.session["status"] = "uploading file"
            data = load_data_from_objfile(file)
            request.session["status"] = "validating file data"
            converter = BulkLoadDataCoverter(mapping[file_type], data)
            if converter.validate_column_name():
                messages.error(request, "File and file type should be same. Kindly upload {0} file or missing fields {1}"
                               .format(file_type,converter.validate_column_name()))
                return redirect('/admin_dashboard/table')
            request.session["status"] = "storing file data to database"
            converter.bulk_save()
            request.session["status"] = "completed"
    messages.success(request,"File uploaded")
    return redirect('/admin_dashboard/table')

def refresh_armor_data(request):
    model=ClaimsDataArmor
    colums_names = [getattr(inst, "name") for inst in model._meta.fields][1:-1]
    claims =model.objects.values_list(*colums_names)
    colums_names_upper=[name.upper() for name in colums_names]
    with open('claim.csv','w') as file:
        file_writer = csv.writer(file)
        file_writer.writerow(colums_names_upper)
        for clam_row in claims:
            file_writer.writerow(clam_row)
    # asyload_to_db_task.delay()


    # asyload_to_db_task.delay(settings.BASE_DIR+"/armor_strat_2.csv","Armor",id=False)

    # for claim in claims:
    #     row.append([claim.field for field in colums_names])

    return redirect('/admin_dashboard/table')

# @user_passes_test(lambda u: u.is_superuser , login_url='/login/')
@login_required(login_url="/login/")
def tablefulldata(request, id):
    if request.method == 'GET':
        armorData = Armor_Start.objects.filter(id_number=id)
        claimsData = ClaimsDataArmor.objects.filter(id_number=id)
        hccData=HCC.objects.filter(icd__in=claimsData.values_list("diag_cd",flat=True))
        drgData=DRG.objects.filter(drg__in=claimsData.values_list("drg_cd",flat=True))
        total_spent = claimsData.aggregate(Sum("total_charges"))
        total_paid=0
        claim_count=0
        for claim in claimsData:
            try:
                paid=float(claim.total_paid)
            except:
                paid=0
            total_paid+=paid
            claim_count+=1
        paginator = Paginator(claimsData, 10)
        page = request.GET.get('page')
        try:
            claim_data = paginator.page(page)
        except PageNotAnInteger:
            claim_data = paginator.page(1)
        except EmptyPage:
            claim_data = paginator.page(paginator.num_pages)
        context = {"claim_data": claim_data, 'page': page}
        context.update({"total_spent":total_spent,"total_paid":total_paid,"claim_count":claim_count})
        context.update({"claimsData":claimsData,"armorData":armorData,"hccData":hccData,"drgData":drgData})
        return render(request, 'PowerBI/viewmore.html', context)



@user_passes_test(lambda u: u.is_superuser , login_url='/login/')
def exportcsv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachement; filename="report.csv"'
    writer = csv.writer(response)
    writer.writerow(['Sr.No.','ID Number','Birth Date','Age'])
    data = MarkAtt.objects.values('studName__VMSAcc').annotate(mark=Sum('attendance'))
    pass


@user_passes_test(lambda u: u.is_superuser , login_url='/login/')
def vendorList(request):
    template = "PowerBI/vendorslist.html"
    if request.method == "GET":
        context = dict()
        cursor = connection.cursor()
        data = cursor.execute(
            "SELECT *  FROM  `auth_user` AS u INNER JOIN `PowerBI_role` AS r ON r.user_id = u.id WHERE  r.role = 1")
        result = cursor.fetchall()
        data_list = list()
        for i in result:
            data_dict = dict()
            data_dict['id'] = i[0]
            data_dict['username'] = i[4]
            data_dict['email'] = i[7]
            data_dict['createdat'] = i[10]
            data_list.append(data_dict)

        # print(result.id)
        context["data"] = data_list
        return render(request, template, context)


@user_passes_test(lambda u: u.is_superuser , login_url='/login/')
def vendorscsv(request):
    template = "PowerBI/vendorscsv.html"
    if request.method == "GET":
        context = dict()
        cursor = connection.cursor()
        data = cursor.execute("SELECT *  FROM  `PowerBI_csvfiles` AS c INNER JOIN `auth_user` AS u ON u.id = c.user_id")
        result = cursor.fetchall()
        data_list = list()
        for i in result:
            data_dict = dict()
            data_dict["id"] = str(i[0])
            data_dict['csv'] = i[1]
            data_dict['filetype'] = i[2]
            data_dict['createdat'] = i[4]
            data_dict['username'] = i[11]
            data_dict["status"] = str(i[5])
            data_dict["uploading_status"] = str(i[6])
            data_list.append(data_dict)
        # print(result.id)
        # data = CsvFiles.objects.filter(user_id_iexact=request.user.id)
        context["data"] = data_list
        return render(request, template, context)


def contact(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        recepient = request.POST.get('email')
        subject = request.POST.get('subject')
        usermessage = request.POST.get('message')
        message = "{0} ({1}) has sent you a new message:\n\n{2}".format(name, recepient, usermessage)
        send_mail('New Enquiry', message, recepient, [EMAIL_HOST_USER])
        # mail_admins(subject, message)
        user_contact = ContactUs.objects.create(name=name,
                                                email=recepient,
                                                subject=subject,
                                                message=message)
        user_contact.save()

    messages.success(request, 'Thanks for contacting us!')
    return redirect('/')

@login_required(login_url="/login/")
# @user_passes_test(lambda u: u.is_superuser , login_url='/login/')
def importcsv(request):
    template = "PowerBI/vendors_dashboard.html"
    if request.method == "GET":
        context = dict()
        data = CsvFiles.objects.filter(user_id=request.user.id)
        context["data"] = data
        return render(request, 'PowerBI/vendors_dashboard.html', context)
    if request.method == "POST":
        mapping = {"Armor": Armor_Start, "VDOC_ADP": VDOC_ADP, "Claims": ClaimsDataArmor, "CPT": CPT, "DRG": DRG}
        try:
            file = request.FILES['file'].file
        except:
            messages.error(request,'Please select file')
            return redirect('/dashboard')
        try:
            file_type = request.POST.get('file_name')
        except:
            messages.error(request,'Please select file type')
            return redirect('/dashboard')
        data = load_data_from_objfile(file)
        if file.name.split(".")[-1] not in ["xlsx", 'csv']:
            messages.error(request, "file format must be xlsx or csv")
            return redirect('/dashboard')

        if request.FILES['file'].name.endswith('.xlsx'):
            csv_upload = CsvFiles.objects.create(csv=request.FILES['file'], file_name=file_type,
                                                 user_id=request.user.id)

        elif request.FILES['file'].name.endswith('.csv'):
            csv_upload = CsvFiles.objects.create(csv=request.FILES['file'], file_name=file_type,
                                                 user_id=request.user.id)

        else:
            messages.error(request, 'Incorrect file extension ....... Kinldy upload only excel and csv files')
            return redirect('/dashboard')

        messages.success(request, 'Successfully uploaded')
        return redirect('/dashboard')


@login_required(login_url="/login/")
def uploaded_csv(request):
    template = "PowerBI/Vendors_allCSV.html"
    if request.method == "GET":
        context = dict()
        data = CsvFiles.objects.filter(user_id=request.user.id)
        print(data)
        for d in data:
            print(d.Status)
        context["data"] = data
        return render(request, 'PowerBI/Vendors_allCSV.html', context)


class SaveFileToDB(LoginRequiredMixin, View):
    login_url = '/login/'
    " load file data to db "
    def get(self, request, id):
        mapping = {"Armor": Armor_Start, "VDOC_ADP": VDOC_ADP, "Claims": ClaimsDataArmor, "CPT": CPT, "DRG": DRG}
        fileobj = CsvFiles.objects.get(pk=id)
        file_name = os.path.join(settings.MEDIA_ROOT, str(fileobj.csv))
        threading.Thread(target=asyload_to_db, args=(file_name, mapping[fileobj.file_name], id)).start()
        fileobj.Status = True
        fileobj.save()
        return redirect('vendorscsv')


class PatientensList(LoginRequiredMixin, View):
    login_url = '/login/'
    def get(self, request):
        id = request.GET.get('id', False)
        if id:
            patientens = Armor_Start.objects.filter(id_number=id).only("id_number", "ami", "chf", "pvd",
                                                                                "cevd", "dementia",
                                                                                "hp", "rend",
                                                                                "canc",
                                                                                "msld",
                                                                                "metacanc",
                                                                                "rheumd",
                                                                                "pud",
                                                                                "mld",
                                                                                "diab",
                                                                                "diabwc",
                                                                                "hp",
                                                                                "rend",
                                                                                "canc",
                                                                                "msld",
                                                                                "metacanc",
                                                                                "aids",
                                                                                "diab",
                                                                                "diabwc", )


            contex = {"patientens": patientens,"id":id}
            return render(request, "PowerBI/patient.html", contex)
        else:
            patientens_list = Armor_Start.objects.all().only("id_number", "ami", "chf", "pvd", "cevd", "dementia",
                                                         "hp", "rend",
                                                         "canc",
                                                         "msld",
                                                         "metacanc",
                                                         "rheumd",
                                                         "pud",
                                                         "mld",
                                                         "diab",
                                                         "diabwc",
                                                         "hp",
                                                         "rend",
                                                         "canc",
                                                         "msld",
                                                         "metacanc",
                                                         "aids",
                                                         "diab",
                                                         "diabwc", )
        paginator = Paginator(patientens_list, 10)
        page = request.GET.get('page')
        try:
            patientens = paginator.page(page)
        except PageNotAnInteger:
            patientens = paginator.page(1)
        except EmptyPage:
            patientens = paginator.page(paginator.num_pages)
        contex = {"patientens": patientens, 'page': page}
        return render(request, "PowerBI/patient.html", contex)

class ClaimPatientList(LoginRequiredMixin,View):
    def get(self,request, id):
        pclaims = ClaimsDataArmor.objects.filter(id_number=id)
        context = {"pclaims": pclaims}
        return render(request,"PowerBI/claimlistpatient.html",context)

@login_required(login_url="/login/")
def main_graph(request):
    date_range_form = DateRangeFilterForm()
    settings = list(filter(None ,tuple(ClaimsDataArmor.objects.values_list("line_setting",flat=True).order_by("line_setting").distinct())))
    ICD = list(filter(None, tuple(ClaimsDataArmor.objects.values_list("diag_cd",flat=True).order_by("diag_cd").distinct())))
    CPT = list(filter(None, tuple(ClaimsDataArmor.objects.values_list("service_code",flat=True).order_by("service_code").distinct())))

    if request.method == 'GET':
        chart=main_graph_drill_down_context_data()
        return render(request, 'PowerBI/maingraph.html',
                      {"chart":chart,
                       "settings":settings,
                       "ICD":ICD,
                       "CPT":CPT,
                       "date_range_form":date_range_form,
                       })
    if request.method == 'POST':
        context=dict()
        line_setting=request.POST.getlist("line_Settings",None)
        icd=request.POST.getlist("icd",None)
        cpt=request.POST.getlist("cpt",None)
        form=DateRangeFilterForm(request.POST)

        if len(line_setting)>0:
            chart = main_graph_filter_settings_drill_down_context_data(line_setting=line_setting)
            context.update({"chart": chart,"line_setting":line_setting})

        if len(icd)>0:
            chart = main_graph_filter_icd_drill_down_context_data(diag_cd=icd)
            context.update({"chart": chart,"Icd":icd})

        if len(cpt)>0:
            chart = main_graph_filter_cpt_drill_down_context_data(service_code=cpt)
            context.update({"chart": chart,"cpt":cpt})
        context.update({"settings":settings,
                       "ICD":ICD,
                       "CPT":CPT})
        return render(request,'PowerBI/maingraph.html',context)


def patient_by_drilldown(request, *args, **kwrgs):
    if request.method == 'GET':
        event_list = list(request.GET.get('event_list').split(","))
        print(event_list)
        month = event_list[1].split('/')[0]
        # print(month)
        # cursor = connection.cursor()
        # data = cursor.execute(
        #     "SELECT ar.id_number, ar.brth_dt, ar.age from PowerBI_Armor_Start AS ar INNER JOIN PowerBI_ClaimsDataArmor"\
        #     " AS cd ON ar.id_number = cd.id_number WHERE year(@cd.date_last_service)="+event_list[0]+\
        #     " AND month(@cd.date_last_service)="+month+" AND cd.provider_name="+event_list[3]+\
        #     " AND cd.group_name="+event_list[2])
        # result = cursor.fetchall()
        # print(result)
        # select 
        data = ClaimsDataArmor.objects.filter(date_last_service__year= event_list[0],
                                              date_last_service__month = month,
                                              group_name = event_list[2],
                                              provider_name = event_list[3] )
        id_num_list = list()
        for i in data:
            id_num_list.append(i.id_number)
            # print("?>>>>>>>",i.id_number)
        print("id list>>>>>>>>",id_num_list)
        print("set id list>>>>>>>>",set(id_num_list))
        armor_data = Armor_Start.objects.filter(id_number__in = set(id_num_list))
        # print(patient_data)
        context=dict()
        patient_data = list()
        for data in armor_data:
            patient_data.append({'id':data.id_number,
                'age':data.age,
                'birthDT':str(data.brth_dt)})
        unique_pateint_id = [dict(t) for t in {tuple(d.items()) for d in patient_data}]
        data = json.dumps(unique_pateint_id)
        return HttpResponse(data)

def provider_graph(request,site):
    provider=ClaimsDataArmor.objects.filter(group_number=site).values_list("provider_name",flat=True).order_by("provider_name").distinct()
    return render(request,"PowerBI/providergraphs.html",{"provider":provider,"site":site})


def site_drill_down(request):
    series_data=list()
    site_data = (
        ClaimsDataArmor.objects.annotate(month=ExtractMonth('date_last_service'), year=ExtractYear("date_last_service"))
        .values_list("month", "year", "group_name")
        .order_by("group_name").distinct()
        .annotate(cost=Sum("total_charges")))
    site_data = (("{0}/{1}".format(x[0],x[1]), x[2], x[3]) for x in site_data)
    # site_data_dict = defaultdict(list)
    # for st in tuple(site_data):
    #     site_data_dict[st[1]].append(st)
    site_data_dict=site_drill_down_data()
    for myr in site_data_dict.keys():
        temp = { "name": str(myr),
                "data": [ {"name":md[0],"y":md[1]} for md in site_data_dict[myr]]}
        series_data.append(temp)
    # print(site_drill_down_data())
    chart= {
        "type": 'column',
        "title": {"text": 'COST FOR EVERY SITE'},
        'xAxis': {"type": 'category'},
        # "xAxis": {"categories":list(site_data_dict.keys())},
        "yAxis": {"title": {"text": 'COST'}},
        "legend": {
        "layout": 'vertical',
        "align": 'right',
        "verticalAlign": 'middle'},
    "series": series_data,
    }
    return JsonResponse(chart)

def main_graph_drill_down_data(request):
    from collections import defaultdict
    series_data=list()
    year_data=ClaimsDataArmor.claims.sum_cost_per_year()
    years=ClaimsDataArmor.objects.values_list("date_last_service__year",flat=True).order_by("date_last_service__year").distinct()
    month_data=(ClaimsDataArmor.objects.annotate(month=ExtractMonth('date_last_service'),year=ExtractYear("date_last_service"))
                .values_list("month","year")
                .order_by("month").distinct()
                .annotate(cost=Sum("total_charges")))
    site_data = (
        ClaimsDataArmor.objects.annotate(month=ExtractMonth('date_last_service'), year=ExtractYear("date_last_service"))
        .values_list("month", "year", "group_name")
        .order_by("group_name").distinct()
        .annotate(cost=Sum("total_charges")))
    provider_data = (
        ClaimsDataArmor.objects.annotate(month=ExtractMonth('date_last_service'), year=ExtractYear("date_last_service"))
        .values_list("month", "year", "group_name", "provider_name")
        .order_by("provider_name").distinct()
        .annotate(cost=Sum("total_charges")))
    month_data,site_data,provider_data=month_data,site_data,provider_data
    month_data_dict = defaultdict(list)
    for md in tuple(month_data):
        month_data_dict[md[1]].append(md)
    for yr in month_data_dict.keys():
        temp={"colorByPoint": True, "name":str(yr)+"-COST:", "id": yr,"data": [{"name":"{0}/{1}".format(md[0],md[1]),"y":md[2],"drilldown":"{0}/{1}".format(md[0],md[1])} for md in month_data_dict[yr]]}
        series_data.append(temp)
    site_data=(("{0}/{1}".format(x[0],x[1]),x[2],x[3]) for x in site_data)
    site_data_dict = defaultdict(list)
    for st in tuple(site_data):
        site_data_dict[st[0]].append(st)
    for myr in site_data_dict.keys():
        temp={"colorByPoint": True, "name":str(myr)+"-SITE COST:", "id": myr,"data": [{"name":"{0}".format(md[1]),"y":md[2],"drilldown":"{0}/{1}".format(md[0],md[1])} for md in site_data_dict[myr]]}
        series_data.append(temp)
    provider_data = (("{0}/{1}/{2}".format(x[0],x[1],x[2]), x[3], x[4]) for x in provider_data)
    provider_data_dict = defaultdict(list)
    for pv in tuple(provider_data):
        provider_data_dict[pv[0]].append(pv)
    for myrs in provider_data_dict.keys():
        temp={"colorByPoint": True, "name":myrs.split("/")[-1]+"-PROVIDER_COST:", "id": myrs,"data": [{"name":"{0}".format(md[1]),"y":md[2],"drilldown":"{0}/{1}".format(md[0],md[1])} for md in provider_data_dict[myrs]]}
        series_data.append(temp)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Total cost'},
        'xAxis': {"type": 'category'},
        'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
        "plotOptions": {"series": {"borderWidth": 0,"dataLabels": {"enabled": False}}},
        'series': [{
            "colorByPoint": True,
            'name': 'YEARS COST',
            'data': [{"name":label,"y":pmpm,"drilldown":label} for label,pmpm in tuple(year_data)]
        }],
        'drilldown':
            {'series':series_data}
    }
    return JsonResponse(chart)



def site_graph_data(request):
    sites=VDOC_ADP.vdocs.get_all_site()
    adp_site_dict=dict()
    claim_site_dict=dict()
    prov_site_dict=dict()
    sum_cost=list()
    datalist=list()
    labels=list()
    for site in sites:
        adp_pmpy = VDOC_ADP.vdocs.filter(site__iexact=site).avg_adp_per_month_per_year()
        adp_pmpy = tuple(map(normalize_adp_date, adp_pmpy))
        adp_pmpy = sorted(adp_pmpy, key=lambda x: x[1])
        adp_dict = {'{}/20{}'.format(x[0], x[1]): x[2] for x in adp_pmpy}
        adp_site_dict[site]=adp_dict
    for site in sites:
        costs_pmpy=ClaimsDataArmor.claims.filter(group_name=site).sum_cost_per_month_per_year()
        prov_cost=ClaimsDataArmor.claims.filter(group_name=site).values_list("provider_name")\
            .order_by("provider_name").distinct().annotate(cost=Sum("total_charges"))
        cost_dict = {'{}/{}'.format(x[0], x[1]): x[2] for x in filter(lambda x: True if x[0] else False, tuple(costs_pmpy))}
        claim_site_dict[site]=cost_dict
        prov_site_dict[site]=list(([x[0],abs(x[1])] for x in prov_cost))
    for site in sites:
        adp_dict=adp_site_dict[site]
        cost_dict=claim_site_dict[site]
        for adp_date in adp_dict.keys():
            try:
                cost = cost_dict[adp_date] / adp_dict[adp_date]
            except:
                cost = 0
            datalist.append(cost)
        data1=datalist.copy()
        labels.append(site)
        sum_cost.append(sum(data1))
        datalist.clear()
    drill_down_data=list(({"id":group,"data":prov_site_dict[group]} for group in prov_site_dict.keys()))
    chart = {
        'chart': {'type': 'pie'},
        'title': {'text': 'Total cost per site'},
        'xAxis': {"categories": labels},
        'legend': {'enabled': True},
        'series':
            [{
            "colorByPoint": True,
            'name': 'cost per member per month',
            'data': [{"name":lab,"y":abs(ct),"drilldown":lab} for lab,ct in zip(labels,sum_cost)]
             }],
        'drilldown':
            {'series': drill_down_data}
    }
    return JsonResponse(chart)


def drill_down_patients_list(request,site,prov):
    down_list=ClaimsDataArmor.claims.filter(group_number=site).filter(provider_name=prov).values_list("id_number").order_by("id_number").distinct()
    patients=Armor_Start.objects.filter(id_number__in=down_list)
    return render(request,"PowerBI/drilldownpatients.html",{"patients":patients,"site":site,"prov":prov})


def patients_profile(request,id):
    context=dict()
    armorData = Armor_Start.objects.filter(id_number=id)
    claimsData = ClaimsDataArmor.claims.filter(id_number=id)
    print(claimsData.sum_cost_per_month_per_year())
    hccData=HCC.objects.filter(icd__in=claimsData.values_list("diag_cd",flat=True))
    drgData=DRG.objects.filter(drg__in=claimsData.values_list("drg_cd",flat=True))
    total_spent = claimsData.aggregate(Sum("total_charges"))
    total_paid=0
    claim_count=0
    for claim in claimsData:
        try:
            paid=float(claim.total_spent)
        except:
            paid=0
        total_paid+=paid
        claim_count+=1
    context.update({"total_spent":total_spent,"total_paid":total_paid,"claim_count":claim_count})
    context.update({"claimsData":claimsData,"armorData":armorData,"hccData":hccData,"drgData":drgData})
    return render(request,"PowerBI/viewmore.html",context)


def patient_by_filter(request):
    template = 'PowerBI/patientByFilter.html'
    if request.method == "GET":
        providernameList = ClaimsDataArmor.objects.values_list('provider_name')
        siteList = ClaimsDataArmor.objects.values_list('group_name')
        context = {
        "providernameList": set(str(i)[2:-3] for i in providernameList),
        "siteList": set(str(i)[2:-3] for i in siteList),
        }
        print(context)
        return render(request, template, context)

    if request.method == "POST":
        provider = request.POST.get('provider')
        site = request.POST.get('site')
        print("provder>>>>>>>>>>>>>>>",provider)
        print("site>>>>>>>>>>>>>>>>>>>>>",site)
        context = dict()
        dataList = list()
        if provider and not site:
            claimsData = ClaimsDataArmor.objects.filter(provider_name = provider)
            idList = [i.id_number for i in claimsData]
            patientData = Armor_Start.objects.filter(id_number__in = idList)
            context['patientData'] = patientData

        elif site and not provider:
            claimsData = ClaimsDataArmor.objects.filter(group_name = site)
            idList = [i.id_number for i in claimsData]
            patientData = Armor_Start.objects.filter(id_number__in = idList)
            context['patientData'] = patientData

        elif provider and site:
            claimsData = ClaimsDataArmor.objects.filter(provider_name = provider, group_name = site)
            idList = [i.id_number for i in claimsData]
            patientData = Armor_Start.objects.filter(id_number__in = idList)
            context['patientData'] = patientData
        else:
            messages.error(request,"please choose provider or site")
            return render(request, template)
        for data in patientData:
            dataList.append({'id':data.id_number,
                            'age':data.age,
                            'birthDT':str(data.brth_dt)})
        data = json.dumps(dataList)
        return HttpResponse(data)

import csv
def dowload_csv_drill_down_patent_list(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
    if request.method == 'GET':
        event_list = list(request.GET.get('event_list').split(","))
        print(event_list)
        month = event_list[1].split('/')[0]
        data = ClaimsDataArmor.objects.filter(date_last_service__year=event_list[0],
                                              date_last_service__month=month,
                                              group_name=event_list[2],
                                              provider_name=event_list[3])

        id_num_list = list()
        for i in data:
            id_num_list.append(i.id_number)
            # print("?>>>>>>>",i.id_number)
        armor_data = Armor_Start.objects.filter(id_number__in=set(id_num_list))
        # print(patient_data)
        context = dict()
        patient_data = list()

        for data in armor_data:
            patient_data.append({'id': data.id_number,
                                 'age': data.age,
                                 'birthDT': str(data.brth_dt)})
        data = json.dumps(patient_data)
        # context['id'] = data.id_number
        # context['age'] = data.age
        # context['birth_date'] = data.brth_dt
        # patient_data.append(context)


    writer = csv.writer(response)
    writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
    writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])

    return response




class UploadJsonDocs(View):
    def get(self,request,*args,**kwargs):
        return render(request,'PowerBI/ui-json-file-uploading.html')


    def post(self,request,*args,**kwargs):
        data_json = request.FILES.get("file")
        dt=json.loads(data_json.read().decode())
        try:
            json_data = JsonDocs.objects.create(
                id = dt.get("id_number",dt.get("$id")),
                doc_type=request.POST.get('file_name'),
                data=dt
            )
            return render(request,'PowerBI/ui-json-file-uploading.html',{"message":"Sucessfully uploaded a json file."})
        except:
            return render(request, 'PowerBI/ui-json-file-uploading.html',
                          {"message": "Error in file uploading"})








# @login_required
# # @ajax_required
# def file_upload_statusbar(request):
#     status = request.session.get("status", "starting uploading")
#     if status == "completed":
#         request.session["status"] = " "
#         return HttpResponse(json.dumps(" "))
#     return HttpResponse(json.dumps(status))
