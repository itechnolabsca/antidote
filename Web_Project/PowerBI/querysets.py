from django.db.models import QuerySet, Avg, Sum
from django.db.models.functions import ExtractMonth, ExtractYear


class VDOC_ADPQuerySet(QuerySet):
    def avg_adp_per_month_per_year(self):
        return self.exclude(monthyear__isnull=True).values_list("monthyear").order_by("monthyear").distinct().annotate(adp_avg=Avg("adp"))

    def get_adp_by_site(self, group_name):
        return self.filter(site_iexact=group_name)

    def get_all_site(self):
        return self.exclude(site__isnull=True).values_list("site", flat=True).order_by("site").distinct()


class ClaimsDataArmorQueryset(QuerySet):
    def sum_cost_per_site_per_month_per_year(self):
        return (self.values_list("date_last_service__month",'date_last_service__year',"group_name")
                .order_by('date_last_service__year',"date_last_service__month","group_name")
                .distinct().annotate(cost=Sum("total_charges")))
    def sum_cost_per_month_per_year(self):
        return (self.annotate(month=ExtractMonth('date_last_service'),year=ExtractYear("date_last_service"))
                     .values_list("month","year").order_by("month","year").distinct()
                     .annotate(cost=Sum("total_charges")))

    def sum_cost_per_year(self):
        return (self.annotate(year=ExtractYear("date_last_service"))
                .values_list("year").order_by("year").distinct().annotate(cost=Sum("total_charges")))

# filter(group_name=sites[0]).values_list("provider_name").order_by("provider_name").distinct().count()


