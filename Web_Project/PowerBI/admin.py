from django.contrib import admin

from .models import *

admin.site.register(Post)
admin.site.register(ContactUs)
# @admin.register(Armor_Start)
# class ArmorAdmin(ImportExportModelAdmin):
#     list_display = ('id_number','ami','chf','pvd','cevd',
# 					'copd','rheumd','pud','mld','diab',
# 					'diabwc','hp','rend','canc','msld',
# 					'metacanc','aids','score','index',
# 					'wscore','windex','birth_dt','age',
# 					'today','age_weight','final_score',)
admin.site.register(CsvFiles)
admin.site.register(ClaimsDataArmor)
admin.site.register(VDOC_ADP)
admin.site.register(HCC)
admin.site.register(ICD10)
admin.site.register(Armor_Start)
admin.site.register(CPT)
admin.site.register(DRG)
admin.site.register(DummyModel)
admin.site.register(Role)