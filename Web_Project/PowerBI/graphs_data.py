import json
from collections import defaultdict

from django.db.models import Q, F
from django.db.models import Sum
from django.db.models.functions import ExtractMonth, ExtractYear
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import *


def maingraphs(request):
    return render(request, "PowerBI/maingraphstest.html")


@csrf_exempt
def datacostperyear(request):
    data = request.body.decode('utf-8')
    data = json.loads(data)

    line_setting = data.get("setting", [])
    icd = data.get("icd", [])
    cpt = data.get("cpt", [])
    if not line_setting and not icd and not cpt:
        armore_obj = ClaimsDataArmor.objects.values_list("date_last_service__year").\
        order_by("date_last_service__year").annotate(cost=Sum("total_charges"))
    else:
        armore_obj = ClaimsDataArmor.objects.all()
        if line_setting:
            armore_obj = armore_obj.filter(line_setting__in=line_setting)
        if icd:
            armore_obj = armore_obj.filter(diag_cd__in=icd)
        if cpt:
            armore_obj = armore_obj.filter(service_code__in=cpt)

        armore_obj = armore_obj.values_list("date_last_service__year").\
            order_by("date_last_service__year").annotate(cost=Sum("total_charges"))

    data = {"data": list(armore_obj)}
    return JsonResponse(data)


@ csrf_exempt
def datacostpermonthyear(request):
    data = request.body.decode('utf-8')
    data_yr = json.loads(data)
    li = data_yr.get("drilldown_list", None)
    line_setting = data_yr.get("setting", [])
    icd = data_yr.get("icd", [])
    icd_filter = Q(diag_cd__in=icd)
    setting_filter = Q(line_setting__in=line_setting)
    condition1 = line_setting == [] and icd == []
    if condition1:
        if len(li) == 1:
            data = (ClaimsDataArmor.objects
                    .filter(date_last_service__year=li[0])
                    .annotate(month=ExtractMonth('date_last_service'))
                    .values_list('month')
                    .order_by('month')
                    .distinct()
                    .annotate(cost=Sum("total_charges")))
        elif len(li) == 2:
            data = (ClaimsDataArmor.objects
                    .filter(
                        Q(date_last_service__year=li[0]) &
                        Q(date_last_service__month=li[1]))
                    .values_list("group_name")
                    .order_by("group_name")
                    .distinct()
                    .annotate(cost=Sum("total_charges"))
                    )
        elif len(li) == 3:
            data = (ClaimsDataArmor.objects
                    .filter(
                        Q(date_last_service__year=li[0]) &
                        Q(date_last_service__month=li[1]) &
                        Q(group_name=li[2])
                    )
                    .values_list("provider_name")
                    .order_by("provider_name")
                    .distinct()
                    .annotate(cost=Sum("total_charges"))
                    )
    else:
        if len(li) == 1:
            data = (ClaimsDataArmor.objects
                    .filter(setting_filter | icd_filter)
                    .filter(date_last_service__year=li[0])
                    .annotate(month=ExtractMonth('date_last_service'))
                    .values_list('month')
                    .order_by('month')
                    .distinct()
                    .annotate(cost=Sum("total_charges")))
        elif len(li) == 2:
            data = (ClaimsDataArmor.objects
                    .filter(setting_filter | icd_filter)
                    .filter(
                        Q(date_last_service__year=li[0]) &
                        Q(date_last_service__month=li[1]))
                    .values_list("group_name")
                    .order_by("group_name")
                    .distinct()
                    .annotate(cost=Sum("total_charges"))
                    )
        elif len(li) == 3:
            data = (ClaimsDataArmor.objects
                    .filter(setting_filter | icd_filter)
                    .filter(
                        Q(date_last_service__year=li[0]) &
                        Q(date_last_service__month=li[1]) &
                        Q(group_name=li[2])
                    )
                    .values_list("provider_name")
                    .order_by("provider_name")
                    .distinct()
                    .annotate(cost=Sum("total_charges"))
                    )

    data = {'data': list(data)}
    return JsonResponse(data)


def setting_filter_data(request):
    settings = list(filter(None, tuple(ClaimsDataArmor.objects
                                       .values_list("line_setting", flat=True)
                                       .order_by("line_setting").distinct())))
    cpt = list(filter(None, tuple(ClaimsDataArmor.objects
                                  .values_list("service_code", flat=True)
                                  .order_by("service_code")
                                  .distinct())))
    return JsonResponse({'data': settings, 'data_cpt': cpt})


def icd_filter_data(request):
    icd = list(filter(None, tuple(ClaimsDataArmor.objects
                                  .values_list("diag_cd", flat=True)
                                  .order_by("diag_cd")
                                  .distinct())))
    # icd = ClaimsDataArmor.objects.all().only('diag_cd')
    # icd = list(set(filter(None, map(lambda x: x.diag_cd, icd))))
    return JsonResponse({'data': icd})


def main_graph_drill_down_context_data():
    series_data = list()
    year_data = ClaimsDataArmor.claims.sum_cost_per_year()
    years = (ClaimsDataArmor.objects
             .values_list('date_last_service__year', flat=True)
             .order_by("date_last_service__year").distinct())
    site_data = tuple(
        ClaimsDataArmor.objects
        .annotate(month=ExtractMonth('date_last_service'), year=ExtractYear("date_last_service"), group_nm=F("group_name"),)
        .values_list("month", 'year', "group_nm")
        .order_by("group_name").distinct().annotate(cost=Sum("total_charges")))

    provider_data = tuple(
        ClaimsDataArmor.objects.values_list(
            "date_last_service__month", 'date_last_service__year', "group_name", "provider_name")
        .order_by("provider_name").distinct()
        .annotate(cost=Sum("total_charges")))

    month_data_dict = {yr: ClaimsDataArmor.claims.filter(
        date_last_service__year=yr).sum_cost_per_month_per_year() for yr in years}
    for yr in month_data_dict.keys():
        temp = {"name": str(yr)+"-COST:", "id": yr, "data": [
            {"name": "{0}/{1}".format(md[0], md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0], md[1])} for md in month_data_dict[yr]]}
        series_data.append(temp)
    site_data = (("{0}/{1}".format(x[0], x[1]), x[2], x[3]) for x in site_data)
    site_data_dict = defaultdict(list)
    for st in site_data:
        site_data_dict[st[0]].append(st)
    for myr in site_data_dict.keys():
        temp = {"name": str(myr)+"-SITE COST:", "id": myr, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0], md[1])} for md in site_data_dict[myr]]}
        series_data.append(temp)
    provider_data = (("{0}/{1}/{2}".format(x[0], x[1], x[2]), x[3], x[4])
                     for x in provider_data)
    provider_data_dict = defaultdict(list)
    for pv in provider_data:
        provider_data_dict[pv[0]].append(pv)
    for myrs in provider_data_dict.keys():
        temp = {"name": myrs.split("/")[-1]+"-PROVIDER_COST:", "id": myrs, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0], md[1])} for md in provider_data_dict[myrs]]}
        series_data.append(temp)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Total cost'},
        'xAxis': {"type": 'category'},
        'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
        "plotOptions": {"series": {"borderWidth": 0, "dataLabels": {"enabled": False}}},
        'series': [{
            'name': 'YEARS COST',
            'data': [{"name": label, "y": pmpm, "drilldown": label} for label, pmpm in tuple(year_data)]
        }],
        'drilldown':
            {'series': series_data}
    }
    return chart


def main_graph_filter_settings_drill_down_context_data(line_setting=None):
    series_data = list()
    line_filter = Q(line_setting__in=line_setting)
    year_data = ClaimsDataArmor.claims.filter().sum_cost_per_year()
    years = (ClaimsDataArmor.objects.filter(line_filter)
             .values_list('date_last_service__year', flat=True)
             .order_by("date_last_service__year").distinct())
    month_year = (ClaimsDataArmor.objects.filter(line_filter)
                  .values_list('date_last_service__month', 'date_last_service__year')
                  .order_by('date_last_service__month', "date_last_service__year").distinct())
    provider_data = (
        ClaimsDataArmor.objects.annotate(month=ExtractMonth(
            'date_last_service'), year=ExtractYear("date_last_service"))
        .values_list("month", "year", "group_name", "provider_name")
        .order_by("provider_name").distinct()
        .annotate(cost=Sum("total_charges")))
    month_data_dict = {yr: ClaimsDataArmor.claims.filter(line_filter).filter(
        date_last_service__year=yr).sum_cost_per_month_per_year() for yr in years}
    for yr in month_data_dict.keys():
        temp = {"name": str(yr)+"-COST:", "id": yr, "data": [
            {"name": "{}/{}".format(myd[0], myd[1]), "y": myd[2], "drilldown":"{}/{}".format(myd[0], myd[1])} for myd in month_data_dict[yr]]}
        series_data.append(temp)
    site_data_dict = {"{}/{}".format(mn, yr): ClaimsDataArmor.claims
                      .filter(line_filter).filter(date_last_service__year=yr).filter(date_last_service__month=mn)
                      .sum_cost_per_site_per_month_per_year() for mn, yr in month_year}
    for myr in site_data_dict.keys():
        temp = {"name": str(myr)+"-SITE COST:", "id": myr, "data": [
            {"name": "{}".format(md[2]), "y": md[3], "drilldown":"{}/{}/{}".format(md[0], md[1], md[2])} for md in site_data_dict[myr]]}
        series_data.append(temp)
    provider_data = (("{0}/{1}/{2}".format(x[0], x[1], x[2]), x[3], x[4])
                     for x in provider_data)
    provider_data_dict = defaultdict(list)
    for pv in provider_data:
        provider_data_dict[pv[0]].append(pv)
    for myrs in provider_data_dict.keys():
        temp = {"name": myrs.split("/")[-1]+"-PROVIDER_COST:", "id": myrs, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0], md[1])} for md in provider_data_dict[myrs]]}
        series_data.append(temp)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Total cost'},
        'xAxis': {"type": 'category'},
        'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
        "plotOptions": {"series": {"borderWidth": 0, "dataLabels": {"enabled": False}}},
        'series': [{
            'name': 'YEARS COST',
            'data': [{"name": label, "y": pmpm, "drilldown": label} for label, pmpm in tuple(year_data)]
        }],
        'drilldown':
            {'series': series_data}
    }
    return chart


def main_graph_filter_icd_drill_down_context_data(diag_cd=None):
    if diag_cd == []:
        return main_graph_drill_down_context_data()
    series_data = list()
    year_data = ClaimsDataArmor.claims.filter(
        Q(diag_cd__in=diag_cd)).sum_cost_per_year()
    years = (ClaimsDataArmor.objects.filter(diag_cd__in=diag_cd).values_list('date_last_service__year', flat=True)
             .order_by("date_last_service__year").distinct())
    site_data = (
        ClaimsDataArmor.objects.filter(diag_cd__in=diag_cd).values_list(
            "date_last_service__month", 'date_last_service__year', "group_name")
        .order_by("group_name").distinct().annotate(cost=Sum("total_charges")))

    provider_data = (
        ClaimsDataArmor.objects.filter(diag_cd__in=diag_cd).values_list("date_last_service__month", 'date_last_service__year', "group_name",
                                                                        "provider_name")
        .order_by("provider_name").distinct()
        .annotate(cost=Sum("total_charges")))

    month_data_dict = {yr: ClaimsDataArmor.claims.filter(date_last_service__year=yr).sum_cost_per_month_per_year() for
                       yr in years}
    for yr in month_data_dict.keys():
        temp = {"name": str(yr) + "-COST:", "id": yr, "data": [
            {"name": "{0}/{1}".format(md[0], md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            month_data_dict[yr]]}
        series_data.append(temp)
    site_data = (("{0}/{1}".format(x[0], x[1]), x[2], x[3]) for x in site_data)
    site_data_dict = defaultdict(list)
    for st in site_data:
        site_data_dict[st[0]].append(st)
    for myr in site_data_dict.keys():
        temp = {"name": str(myr) + "-SITE COST:", "id": myr, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            site_data_dict[myr]]}
        series_data.append(temp)
    provider_data = (("{0}/{1}/{2}".format(x[0], x[1], x[2]), x[3], x[4])
                     for x in provider_data)
    provider_data_dict = defaultdict(list)
    for pv in provider_data:
        provider_data_dict[pv[0]].append(pv)
    for myrs in provider_data_dict.keys():
        temp = {"name": myrs.split("/")[-1] + "-PROVIDER_COST:", "id": myrs, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            provider_data_dict[myrs]]}
        series_data.append(temp)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Total cost'},
        'xAxis': {"type": 'category'},
        'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
        "plotOptions": {"series": {"borderWidth": 0, "dataLabels": {"enabled": False}}},
        'series': [{
            'name': 'YEARS COST',
            'data': [{"name": label, "y": pmpm, "drilldown": label} for label, pmpm in tuple(year_data)]
        }],
        'drilldown':
            {'series': series_data}
    }
    return chart


def main_graph_filter_cpt_drill_down_context_data(service_code=None):
    if service_code == []:
        return main_graph_drill_down_context_data()
    series_data = list()
    year_data = ClaimsDataArmor.claims.filter(
        Q(service_code__in=service_code)).sum_cost_per_year()
    years = (ClaimsDataArmor.objects.filter(service_code__in=service_code).values_list('date_last_service__year', flat=True)
             .order_by("date_last_service__year").distinct())
    site_data = (
        ClaimsDataArmor.objects.filter(service_code__in=service_code).values_list(
            "date_last_service__month", 'date_last_service__year', "group_name")
        .order_by("group_name").distinct().annotate(cost=Sum("total_charges")))

    provider_data = (
        ClaimsDataArmor.objects.filter(service_code__in=service_code).values_list("date_last_service__month", 'date_last_service__year', "group_name",
                                                                                  "provider_name")
        .order_by("provider_name").distinct()
        .annotate(cost=Sum("total_charges")))

    month_data_dict = {yr: ClaimsDataArmor.claims.filter(date_last_service__year=yr).sum_cost_per_month_per_year() for
                       yr in years}
    for yr in month_data_dict.keys():
        temp = {"name": str(yr) + "-COST:", "id": yr, "data": [
            {"name": "{0}/{1}".format(md[0], md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            month_data_dict[yr]]}
        series_data.append(temp)
    site_data = (("{0}/{1}".format(x[0], x[1]), x[2], x[3]) for x in site_data)
    site_data_dict = defaultdict(list)
    for st in site_data:
        site_data_dict[st[0]].append(st)
    for myr in site_data_dict.keys():
        temp = {"name": str(myr) + "-SITE COST:", "id": myr, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            site_data_dict[myr]]}
        series_data.append(temp)
    provider_data = (("{0}/{1}/{2}".format(x[0], x[1], x[2]), x[3], x[4])
                     for x in provider_data)
    provider_data_dict = defaultdict(list)
    for pv in provider_data:
        provider_data_dict[pv[0]].append(pv)
    for myrs in provider_data_dict.keys():
        temp = {"name": myrs.split("/")[-1] + "-PROVIDER_COST:", "id": myrs, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            provider_data_dict[myrs]]}
        series_data.append(temp)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Total cost'},
        'xAxis': {"type": 'category'},
        'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
        "plotOptions": {"series": {"borderWidth": 0, "dataLabels": {"enabled": False}}},
        'series': [{
            'name': 'YEARS COST',
            'data': [{"name": label, "y": pmpm, "drilldown": label} for label, pmpm in tuple(year_data)]
        }],
        'drilldown':
            {'series': series_data}
    }
    return chart
