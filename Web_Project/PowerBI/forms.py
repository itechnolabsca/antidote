from django import forms

YEAR_CHOICES = ["2014","2015","2016","2017","2018","2019"]


class DateRangeFilterForm(forms.Form):
    date_from = forms.DateField(widget=forms.SelectDateWidget(years=YEAR_CHOICES))
    date_to = forms.DateField(widget=forms.SelectDateWidget(years=YEAR_CHOICES))
