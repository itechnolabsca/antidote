from django.db.models import Sum
from django.db.models.functions import ExtractMonth, ExtractYear

from .models import VDOC_ADP, ClaimsDataArmor
from .utils import load_data_from_file, BulkLoadDataCoverter


def asyload_to_db(file_name, model, id):
    data = load_data_from_file(file_name)
    converter = BulkLoadDataCoverter(model, data)
    converter.create_or_update(id=id)


def normalize_adp_date(adp_data):
    month = int(adp_data[0].split(".")[0][1:])
    year = int(adp_data[0].split(".")[-1])
    return month,year,adp_data[-1]

def site_drill_down_data():
    sites = VDOC_ADP.vdocs.get_all_site()
    adp_site_dict = dict()
    claim_site_dict = dict()
    data_site_pmpm = dict()
    datalist = list()
    datalist2 = list()
    final_data= list()
    for site in sites:
        adp_pmpy = VDOC_ADP.vdocs.filter(site__iexact=site).avg_adp_per_month_per_year()
        adp_pmpy = tuple(map(normalize_adp_date, adp_pmpy))
        adp_pmpy = sorted(adp_pmpy, key=lambda x: x[1])
        adp_dict = {'{}/20{}'.format(x[0], x[1]): x[2] for x in adp_pmpy}
        adp_site_dict[site] = adp_dict
    for site in sites:
        costs_pmpy = ClaimsDataArmor.claims.filter(group_name=site).sum_cost_per_month_per_year()
        cost_dict = {'{}/{}'.format(x[0], x[1]): x[2] for x in
                     filter(lambda x: True if x[0] else False, tuple(costs_pmpy))}
        claim_site_dict[site] = cost_dict
    for site in sites:
        adp_dict = adp_site_dict[site]
        cost_dict = claim_site_dict[site]
        for adp_date in adp_dict.keys():
            try:
                cost = cost_dict[adp_date] / adp_dict[adp_date]
            except:
                cost = 0
            datalist.append([adp_date,cost])
        data1 = datalist.copy()
        data_site_pmpm.update({site:data1})
        datalist.clear()
    return data_site_pmpm

# def main_graph_drill_down_context_data():
#     from collections import defaultdict
#     series_data = list()
#     year_data = ClaimsDataArmor.claims.sum_cost_per_year()
#     years = (ClaimsDataArmor.objects.values_list('date_last_service__year', flat=True)
#                   .order_by("date_last_service__year").distinct())
#     site_data = (
#         ClaimsDataArmor.objects.annotate(month=ExtractMonth(
#             'date_last_service'), year=ExtractYear("date_last_service"))
#         .values_list("month", "year", "group_name")
#         .order_by("group_name").distinct()
#         .annotate(cost=Sum("total_charges")))
#     provider_data = (
#         ClaimsDataArmor.objects.annotate(month=ExtractMonth(
#             'date_last_service'), year=ExtractYear("date_last_service"))
#         .values_list("month", "year", "group_name", "provider_name")
#         .order_by("provider_name").distinct()
#         .annotate(cost=Sum("total_charges")))
#     month_data_dict = {yr: ClaimsDataArmor.claims.filter(date_last_service__year=yr).sum_cost_per_month_per_year() for yr in years}
#     for yr in month_data_dict.keys():
#         temp = {"name": str(yr)+"-COST:", "id": yr, "data": [
#             {"name": "{0}/{1}".format(md[0],md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0],md[1])} for md in month_data_dict[yr]]}
#         series_data.append(temp)
#     site_data = (("{0}/{1}".format(x[0],x[1]), x[2], x[3]) for x in site_data)
#     site_data_dict = defaultdict(list)
#     for st in site_data:
#         site_data_dict[st[0]].append(st)
#     for myr in site_data_dict.keys():
#         temp = {"name": str(myr)+"-SITE COST:", "id": myr, "data": [
#             {"name": "{0}".format(md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0],md[1])} for md in site_data_dict[myr]]}
#         series_data.append(temp)
#     provider_data = (("{0}/{1}/{2}".format(x[0],x[1],x[2]), x[3], x[4])
#                      for x in provider_data)
#     provider_data_dict = defaultdict(list)
#     for pv in provider_data:
#         provider_data_dict[pv[0]].append(pv)
#     for myrs in provider_data_dict.keys():
#         temp = {"name": myrs.split("/")[-1]+"-PROVIDER_COST:", "id": myrs, "data": [
#             {"name": "{0}".format(md[1]), "y": md[2], "drilldown":"{0}/{1}".format(md[0],md[1])} for md in provider_data_dict[myrs]]}
#         series_data.append(temp)
#     chart = {
#         'chart': {'type': 'column'},
#         'title': {'text': 'Total cost'},
#         'xAxis': {"type": 'category'},
#         'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
#         "plotOptions": {"series": {"borderWidth": 0, "dataLabels": {"enabled": False}}},
#         'series': [{
#             'name': 'YEARS COST',
#             'data': [{"name": label, "y": pmpm, "drilldown": label} for label, pmpm in tuple(year_data)]
#         }],
#         'drilldown':
#             {'series': series_data}
#     }
#     return chart




def main_graph_filter_icd_drill_down_context_data(diag_cd=None):
    from collections import defaultdict
    if diag_cd == "All":
        return main_graph_drill_down_context_data()
    series_data = list()
    year_data = ClaimsDataArmor.claims.filter(diag_cd=diag_cd).sum_cost_per_year()
    years = (ClaimsDataArmor.objects.filter(diag_cd=diag_cd)
             .values_list('date_last_service__year', flat=True)
             .order_by("date_last_service__year").distinct())
    month_year = (ClaimsDataArmor.objects.filter(diag_cd=diag_cd)
                  .values_list('date_last_service__month', 'date_last_service__year')
                  .order_by('date_last_service__month', "date_last_service__year").distinct())
    provider_data = (
        ClaimsDataArmor.objects.annotate(month=ExtractMonth(
            'date_last_service'), year=ExtractYear("date_last_service"))
            .values_list("month", "year", "group_name", "provider_name")
            .order_by("provider_name").distinct()
            .annotate(cost=Sum("total_charges")))
    month_data_dict = {yr: ClaimsDataArmor.claims.filter(diag_cd=diag_cd).filter(
        date_last_service__year=yr).sum_cost_per_month_per_year() for yr in years}
    for yr in month_data_dict.keys():
        temp = {"name": str(yr) + "-COST:", "id": yr, "data": [
            {"name": "{}/{}".format(myd[0], myd[1]), "y": myd[2], "drilldown": "{}/{}".format(myd[0], myd[1])} for myd
            in month_data_dict[yr]]}
        series_data.append(temp)
    site_data_dict = {"{}/{}".format(mn, yr): ClaimsDataArmor.claims
        .filter(diag_cd=diag_cd).filter(date_last_service__year=yr).filter(date_last_service__month=mn)
        .sum_cost_per_site_per_month_per_year() for mn, yr in month_year}
    for myr in site_data_dict.keys():
        temp = {"name": str(myr) + "-SITE COST:", "id": myr, "data": [
            {"name": "{}".format(md[2]), "y": md[3], "drilldown": "{}/{}/{}".format(md[0], md[1], md[2])} for md in
            site_data_dict[myr]]}
        series_data.append(temp)
    provider_data = (("{0}/{1}/{2}".format(x[0],x[1],x[2]), x[3], x[4])
                     for x in provider_data)
    provider_data_dict = defaultdict(list)
    for pv in provider_data:
        provider_data_dict[pv[0]].append(pv)
    for myrs in provider_data_dict.keys():
        temp = {"name": myrs.split("/")[-1] + "-PROVIDER_COST:", "id": myrs, "data": [
            {"name": "{0}".format(md[1]), "y": md[2], "drilldown": "{0}/{1}".format(md[0], md[1])} for md in
            provider_data_dict[myrs]]}
        series_data.append(temp)
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Total cost'},
        'xAxis': {"type": 'category'},
        'yAxis': {'title': {'text': 'Cost Per Member Per Month'}},
        "plotOptions": {"series": {"borderWidth": 0, "dataLabels": {"enabled": False}}},
        'series': [{
            'name': 'YEARS COST',
            'data': [{"name": label, "y": pmpm, "drilldown": label} for label, pmpm in tuple(year_data)]
        }],
        'drilldown':
            {'series': series_data}
    }
    return chart