# from django.shortcuts import render, redirect
# from django.contrib import messages
# from .forms import UserRegisterForm
# from django.contrib.auth.decorators import login_required


# def register(request):
#     if request.method == 'POST':
#         form = UserRegisterForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             messages.success(request, 'Account Created for {username}!'.format(username=username))
#             return redirect('home-page')
#     else:
#         form = UserRegisterForm()
#     return render(request, 'users/register.html',{'form':form})

# @login_required
# def profile(request):
#     return render(request, 'users/profile.html')


# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from PowerBI.models import *
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as django_logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from .forms import *


def loginView(request):
    form = LoginForm(request.POST or None)

    msg = None

    if request.method == "POST":
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                if not user.is_superuser:
                    role_data = Role.objects.get(user_id = user.id)
                    if int(role_data.role) == 1:
                        login(request, user)
                        return redirect("/dashboard")
                    else:
                        login(request, user)
                        return redirect("/")
                else:
                    login(request, user)
                    return redirect("/admin_dashboard")
            else:    
                msg = 'Invalid credentials'    
        else:
            msg = 'Error validating the form'    

    return render(request, "users/login.html", {"form": form, "msg" : msg})


def register(request):
    if request.method == 'GET':
        if request.user.is_superuser:
            form = SignUpFormbyAdmin(request.POST)
            return render(request, "users/register_by_admin.html", {"form": form})
        else:
            form = SignUpForm(request.POST)
            return render(request, "users/register.html", {"form": form})
    msg     = None
    success = False

    if request.method == "POST":
        if request.user.is_superuser:
            form = SignUpFormbyAdmin(request.POST)
        else:
            form = SignUpForm(request.POST)
        # form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            role = form.cleaned_data.get("role")
            raw_password = form.cleaned_data.get("password1")
            registeruser = authenticate(username=username, password=raw_password)
            msg     = 'User created.'
            success = True
            # role = Role.objects.create(user_id = registeruser.id, role = role)
            if request.user.is_superuser:
                role = Role.objects.create(user_id = registeruser.id, role = role)
                # messages.success(request,'Successfully created user')
                return render(request, "users/register_by_admin.html", {"form": form, "msg" : msg, "success" : success })
            else:
                role = Role.objects.create(user_id = registeruser.id, role = 2)
            return redirect("/login")

        else:
            msg = 'Something went wrong. Please try again'    
    else:
        if request.user.is_superuser:
            form = SignUpFormbyAdmin()
        else:
            form = SignUpForm()
    if not request.user.is_superuser:
        return render(request, "users/register.html", {"form": form, "msg" : msg, "success" : success })
    else:
        return render(request, "users/register_by_admin.html", {"form": form, "msg" : msg, "success" : success })        


@login_required(login_url="/login/")
def logout(request):
    django_logout(request)
    return redirect('/login')
