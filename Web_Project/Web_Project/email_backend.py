from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q

User = get_user_model()


class EmailBackend(ModelBackend):
    def authenticate(self, request, **kwargs):
        try:
            user = User.objects.get(Q(email=kwargs.get("email")) | Q(username=kwargs.get("username")))
        except User.DoesNotExist:
            return None
        if user.check_password(kwargs.get("password")) and user.is_authenticated:
            return user
        return None
